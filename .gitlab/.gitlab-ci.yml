stages:
  - build
  - test
  - publish
  - assert
  - merge

include:
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/push-main-branch-to-github@main
    inputs:
      stage: publish
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/push-development-branch-to-github@main
    inputs:
      stage: publish
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/push-tag-to-github@main
    inputs:
      stage: publish
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/check-configuration-formatting@main
    inputs:
      stage: assert
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/check-configuration-editorconfig-updates@main
    inputs:
      stage: assert
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/check-configuration-gitattributes-updates@main
    inputs:
      stage: assert
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/check-configuration-gitignore-updates@main
    inputs:
      stage: assert
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/check-docs-license-updates@main
    inputs:
      stage: assert
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/check-docs-contributing-updates@main
    inputs:
      stage: assert
  - component: gitlab.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.GitLab.Extensions/auto-merge-main-branch@main
    inputs:
      stage: merge

variables:
  GITHUB_REPOSITORY_URL:
    description: "The GitHub repository to push branch/tag to. Must be in the SSH format."
    value: "git@github.com:AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.Infrastructure.Data.git"
  GITHUB_REPOSITORY_SSH_PRIVATE_KEY:
    description: "The SSH private key file added to the GitHub repository to authenticate push."
  GITLAB_REPOSITORY_URL:
    description: "The GitLab repository to push branch/tag to. Must be in the SSH format."
    value: "git@gitlab.com:AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.Infrastructure.Data.git"
  GITLAB_REPOSITORY_SSH_PRIVATE_KEY:
    description: "The SSH private key file added to the GitLab repository to authenticate push."
  CI_DEFAULT_MAIN_BRANCH:
    description: "The 'main' branch name. To be used alongside the CI_DEFAULT_BRANCH variable."
    value: "main"
  CI_DEFAULT_DEVELOPMENT_BRANCH:
    description: "The 'development' branch name. To be used alongside the CI_DEFAULT_BRANCH variable."
    value: "development"
  CI_AUTOMATICALLY_CREATED_BRANCH_PREFIX:
    description: "The 'auto-created' branch name prefix. To be used to skip jobs for certain branches."
    value: "auto-created"

# Build the 'MusicLibrarySuite.Infrastructure.Data' NuGet package project
build_musiclibrarysuite_infrastructure_data_nuget_package_project:
  stage: build
  image: $CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX/docker:25
  services:
    - name: $CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX/docker:25-dind
      alias: docker
  script: |
    docker build \
      --file ./Source/MusicLibrarySuite.Infrastructure.Data/Dockerfile \
      --build-arg NUGET_USERNAME="gitlab-ci-token" \
      --build-arg NUGET_CLEARTEXTPASSWORD="${CI_JOB_TOKEN}" \
      --build-arg NUGET_CONFIGURATIONFILENAME="NuGet.GitLab.Template.config" \
      ./Source

# Build the 'MusicLibrarySuite.Infrastructure.Data.Sqlite' NuGet package project
build_musiclibrarysuite_infrastructure_data_sqlite_nuget_package_project:
  stage: build
  image: $CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX/docker:25
  services:
    - name: $CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX/docker:25-dind
      alias: docker
  script: |
    docker build \
      --file ./Source/MusicLibrarySuite.Infrastructure.Data.Sqlite/Dockerfile \
      --build-arg NUGET_USERNAME="gitlab-ci-token" \
      --build-arg NUGET_CLEARTEXTPASSWORD="${CI_JOB_TOKEN}" \
      --build-arg NUGET_CONFIGURATIONFILENAME="NuGet.GitLab.Template.config" \
      ./Source

# Build the 'MusicLibrarySuite.Infrastructure.Data.SqlServer' NuGet package project
build_musiclibrarysuite_infrastructure_data_sqlserver_nuget_package_project:
  stage: build
  image: $CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX/docker:25
  services:
    - name: $CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX/docker:25-dind
      alias: docker
  script: |
    docker build \
      --file ./Source/MusicLibrarySuite.Infrastructure.Data.SqlServer/Dockerfile \
      --build-arg NUGET_USERNAME="gitlab-ci-token" \
      --build-arg NUGET_CLEARTEXTPASSWORD="${CI_JOB_TOKEN}" \
      --build-arg NUGET_CONFIGURATIONFILENAME="NuGet.GitLab.Template.config" \
      ./Source
