# MusicLibrarySuite.Infrastructure.Data

A package providing utility classes and extension methods for the data access layer represented by the Entity Framework Core object–relational mapper.
