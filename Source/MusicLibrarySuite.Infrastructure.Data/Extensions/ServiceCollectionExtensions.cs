using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;

using MusicLibrarySuite.Infrastructure.Data.Services;
using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.Infrastructure.Data.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IServiceCollection" /> interface.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Registers the specified context and context factory as services in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <typeparam name="TContextService">The class that will be used to resolve the context from the container.</typeparam>
    /// <typeparam name="TContextImplementation">The concrete implementation type to create.</typeparam>
    /// <param name="services">The <see cref="IServiceCollection" /> collection to add services to.</param>
    /// <param name="optionsAction">The optional action to configure the <see cref="DbContextOptions" /> options for the context.</param>
    /// <param name="contextLifetime">The lifetime with which to register the <see cref="DbContext" /> service in the container.</param>
    /// <param name="contextFactoryLifetime">The lifetime with which to register the <see cref="DbContextFactory{TContext}" /> service in the container.</param>
    /// <param name="optionsLifetime">The lifetime with which to register the <see cref="DbContextOptions" /> service in the container.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddDbContextFactory<TContextService, TContextImplementation>(
        this IServiceCollection services,
        Action<DbContextOptionsBuilder>? optionsAction = null,
        ServiceLifetime contextLifetime = ServiceLifetime.Scoped,
        ServiceLifetime contextFactoryLifetime = ServiceLifetime.Singleton,
        ServiceLifetime optionsLifetime = ServiceLifetime.Singleton)
        where TContextService : DbContext
        where TContextImplementation : DbContext, TContextService
    {
        services.AddDbContext<TContextImplementation>(optionsAction, contextLifetime, optionsLifetime);
        services.AddDbContextFactory<TContextImplementation>(optionsAction, contextFactoryLifetime);

        services.AddScoped<TContextService>(serviceProvider => serviceProvider.GetRequiredService<TContextImplementation>());
        services.AddSingleton<IDbContextFactory<TContextService>, DbContextFactoryProxy<TContextService, TContextImplementation>>();

        return services;
    }

    /// <summary>
    /// Registers the <see cref="IDbContextProvider{TContext}" /> and <see cref="IDbContextScopeFactory{TContext}" /> services in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <typeparam name="TContext">The <see cref="DbContext" /> type of the instance for the <see cref="IDbContextProvider{TContext}" /> to provide access to.</typeparam>
    /// <param name="services">The <see cref="IServiceCollection" /> collection to add services to.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddDbContextScopeFactory<TContext>(this IServiceCollection services)
        where TContext : DbContext
    {
        services.AddScoped<DbContextProviderSource<TContext>>();

        services.AddScoped<IDbContextProvider<TContext>>(serviceProvider => serviceProvider.GetRequiredService<DbContextProviderSource<TContext>>());
        services.AddScoped<IDbContextProviderSource<TContext>>(serviceProvider => serviceProvider.GetRequiredService<DbContextProviderSource<TContext>>());

        services.AddScoped<IDbContextScopeFactory<TContext>, DbContextScopeFactory<TContext>>();

        return services;
    }
}
