using System.Threading;
using System.Threading.Tasks;

namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Encapsulates an asynchronous repository action.
/// </summary>
/// <remarks>The repository instance to execute the action on and action parameters should be provided via closures.</remarks>
/// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
/// <returns>
/// A task that represents the asynchronous repository operation.
/// </returns>
public delegate Task AsyncRepositoryAction(CancellationToken cancellationToken = default);
