using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Represents a handle for an external <see cref="DbConnection" /> connection that manages its state.
/// <para>When a handle is created, it opens the provided connection if necessary, and when the same handle is disposed, the connection is only closed if it was opened by the handle.</para>
/// </summary>
/// <remarks>Consider utilizing the <c>using</c> or <c>await using</c> statement to avoid connection lifetime mismanagement.</remarks>
public class DbConnectionHandle : IDisposable, IAsyncDisposable
{
    private readonly DbConnection m_connection;
    private readonly bool m_closeConnectionOnDispose;
    private bool m_disposed;

    /// <summary>
    /// Initializes a new instance of the <see cref="DbConnectionHandle" /> type using the specified <see cref="DbConnection" /> connection object.
    /// </summary>
    /// <param name="connection">The <see cref="DbConnection" /> connection object.</param>
    public DbConnectionHandle(DbConnection connection)
        : this(connection, false)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DbConnectionHandle" /> type using the specified <see cref="DbConnection" /> connection object and overridden initial state.
    /// </summary>
    /// <param name="connection">The <see cref="DbConnection" /> connection object.</param>
    /// <param name="closeConnectionOnDispose">Specifies whether the connection should be closed when the handle gets disposed.</param>
    public DbConnectionHandle(DbConnection connection, bool closeConnectionOnDispose)
    {
        m_connection = connection;
        m_closeConnectionOnDispose = closeConnectionOnDispose;
        m_disposed = false;

        if (connection.State == ConnectionState.Closed)
        {
            m_connection.Open();
            m_closeConnectionOnDispose = true;
        }
    }

    /// <summary>
    /// Gets the <see cref="DbConnection" /> connection object managed by the current handle.
    /// </summary>
    public DbConnection Connection
    {
        get
        {
            return m_disposed
                ? throw new ObjectDisposedException("The connection handle has already been disposed.", (Exception?)null)
                : m_connection;
        }
    }

    /// <inheritdoc />
    public void Dispose()
    {
        Dispose(dispose: true);
        GC.SuppressFinalize(this);
    }

    /// <inheritdoc />
    public async ValueTask DisposeAsync()
    {
        await DisposeAsync(dispose: true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    /// <param name="dispose">Specifies whether the <see cref="Dispose(bool)" /> method was called as part of controlled resource releasing and not garbage collection.</param>
    protected virtual void Dispose(bool dispose)
    {
        if (!m_disposed)
        {
            if (dispose && m_closeConnectionOnDispose)
            {
                m_connection.Close();
            }

            m_disposed = true;
        }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources asynchronously.
    /// </summary>
    /// <param name="dispose">Specifies whether the <see cref="Dispose(bool)" /> method was called as part of controlled resource releasing and not garbage collection.</param>
    /// <returns>A task that represents the asynchronous dispose operation.</returns>
    protected virtual async ValueTask DisposeAsync(bool dispose)
    {
        if (!m_disposed)
        {
            if (dispose && m_closeConnectionOnDispose)
            {
                await m_connection.CloseAsync();
            }

            m_disposed = true;
        }
    }
}
