using Microsoft.EntityFrameworkCore;

namespace MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

/// <summary>
/// Defines a <see cref="DbContext" /> instance provider source.
/// </summary>
/// <typeparam name="TContext">The <see cref="DbContext" /> type of the instance to provide access to.</typeparam>
internal interface IDbContextProviderSource<TContext>
    where TContext : DbContext
{
    /// <summary>
    /// Gets or sets the <see cref="DbContext" /> instance.
    /// </summary>
    public TContext Context { get; set; }
}
