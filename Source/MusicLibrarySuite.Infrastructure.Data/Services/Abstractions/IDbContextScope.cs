using System;

using Microsoft.EntityFrameworkCore;

namespace MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

/// <summary>
/// Defines a scope substituting the <see cref="DbContext" /> instance provided by the <see cref="IDbContextProvider{TContext}" /> instance.
/// </summary>
/// <typeparam name="TContext">The <see cref="DbContext" /> type of the <see cref="IDbContextProvider{TContext}" /> instance.</typeparam>
public interface IDbContextScope<out TContext> : IDisposable
    where TContext : DbContext
{
    /// <summary>
    /// Gets the <see cref="DbContext" /> instance associated with the scope.
    /// </summary>
    public TContext Context { get; }
}
