using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

namespace MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

/// <summary>
/// Defines a factory for creating <see cref="IDbContextScope{TContext}" /> instances.
/// </summary>
/// <typeparam name="TContext">The <see cref="DbContext" /> type of the <see cref="IDbContextScope{TContext}" /> instance.</typeparam>
public interface IDbContextScopeFactory<TContext>
    where TContext : DbContext
{
    /// <summary>
    /// Substitutes the <see cref="DbContext" /> instance for a single repository action.
    /// </summary>
    /// <param name="repositoryAction">The repository action to execute.</param>
    /// <param name="context">The <see cref="DbContext" /> instance to substitute the underlying instance with.</param>
    public void WithDbContext(RepositoryAction repositoryAction, TContext context);

    /// <summary>
    /// Substitutes the <see cref="DbContext" /> instance for a single repository query.
    /// </summary>
    /// <typeparam name="TResult">The type of the return value of the repository query.</typeparam>
    /// <param name="repositoryQuery">The repository query to execute.</param>
    /// <param name="context">The <see cref="DbContext" /> instance to substitute the underlying instance with.</param>
    /// <returns>The return value of the repository query.</returns>
    public TResult WithDbContext<TResult>(RepositoryQuery<TResult> repositoryQuery, TContext context);

    /// <summary>
    /// Substitutes the <see cref="DbContext" /> instance for a single asynchronous repository action.
    /// </summary>
    /// <param name="repositoryAction">The asynchronous repository action to execute.</param>
    /// <param name="context">The <see cref="DbContext" /> instance to substitute the underlying instance with.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// A task that represents the asynchronous repository operation.
    /// </returns>
    public Task WithDbContextAsync(AsyncRepositoryAction repositoryAction, TContext context, CancellationToken cancellationToken = default);

    /// <summary>
    /// Substitutes the <see cref="DbContext" /> instance for a single asynchronous repository query.
    /// </summary>
    /// <typeparam name="TResult">The type of the return value of the repository query.</typeparam>
    /// <param name="repositoryQuery">The asynchronous repository query to execute.</param>
    /// <param name="context">The <see cref="DbContext" /> instance to substitute the underlying instance with.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// A task that represents the asynchronous repository operation.
    /// The task result contains the return value of the repository query.
    /// </returns>
    public Task<TResult> WithDbContextAsync<TResult>(AsyncRepositoryQuery<TResult> repositoryQuery, TContext context, CancellationToken cancellationToken = default);

    /// <summary>
    /// Creates a new <see cref="IDbContextScope{TContext}" /> instance that substitutes the <see cref="DbContext" /> instance for all operations performed during the scope's lifetime.
    /// </summary>
    /// <remarks> Dispose the scope to cancel the substitution. If multiple nested scopes are created, they must be disposed in the reverse order as they were created.</remarks>
    /// <param name="context">The <see cref="DbContext" /> instance to substitute the original instance with.</param>
    /// <returns>A new <see cref="IDbContextScope{TContext}" /> instance.</returns>
    public IDbContextScope<TContext> CreateDbContextScope(TContext context);
}
