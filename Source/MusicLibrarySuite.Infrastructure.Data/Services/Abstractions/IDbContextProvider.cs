using Microsoft.EntityFrameworkCore;

namespace MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

/// <summary>
/// Defines a <see cref="DbContext" /> instance provider.
/// </summary>
/// <typeparam name="TContext">The <see cref="DbContext" /> type of the instance to provide access to.</typeparam>
public interface IDbContextProvider<out TContext>
    where TContext : DbContext
{
    /// <summary>
    /// Gets the <see cref="DbContext" /> instance.
    /// </summary>
    public TContext Context { get; }
}
