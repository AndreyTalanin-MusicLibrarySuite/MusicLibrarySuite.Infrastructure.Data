using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.Infrastructure.Data.Services;

/// <summary>
/// Represents a <see cref="DbContext" /> instance provider source.
/// </summary>
/// <typeparam name="TContext">The <see cref="DbContext" /> type of the instance to provide access to.</typeparam>
internal class DbContextProviderSource<TContext> : IDbContextProviderSource<TContext>, IDbContextProvider<TContext>
    where TContext : DbContext
{
    /// <inheritdoc />
    public TContext Context { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="DbContextProviderSource{TContext}" /> type using the specified services.
    /// </summary>
    /// <param name="context">The <see cref="DbContext" /> instance to provide access to.</param>
    public DbContextProviderSource(TContext context)
    {
        Context = context;
    }
}
