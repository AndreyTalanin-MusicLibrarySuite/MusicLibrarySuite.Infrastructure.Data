using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.Infrastructure.Data.Services;

/// <summary>
/// Represents a scope substituting the <see cref="DbContext" /> instance provided by the <see cref="IDbContextProvider{TContext}" /> instance.
/// </summary>
/// <typeparam name="TContext">The <see cref="DbContext" /> type of the <see cref="IDbContextProvider{TContext}" /> instance.</typeparam>
internal sealed class DbContextScope<TContext> : IDbContextScope<TContext>
    where TContext : DbContext
{
    private readonly DbContextScopeFactory<TContext> m_contextScopeFactory;
    private bool m_disposed;

    /// <inheritdoc />
    public TContext Context { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="DbContextScope{TContext}" /> type using the specified services.
    /// </summary>
    /// <param name="context">The <see cref="DbContext" /> instance to substitute the original instance with.</param>
    /// <param name="contextScopeFactory">The <see cref="DbContextScopeFactory{TContext}" /> instance that will manage the scope being created.</param>
    public DbContextScope(TContext context, DbContextScopeFactory<TContext> contextScopeFactory)
    {
        m_contextScopeFactory = contextScopeFactory;
        m_disposed = false;

        Context = context;
    }

    /// <inheritdoc />
    public void Dispose()
    {
        if (!m_disposed)
        {
            m_contextScopeFactory.DisposeDbContextScope(this);
            m_disposed = true;
        }
    }
}
