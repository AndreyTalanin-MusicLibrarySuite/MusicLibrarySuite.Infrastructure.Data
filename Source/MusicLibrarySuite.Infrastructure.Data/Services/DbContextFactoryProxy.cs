using Microsoft.EntityFrameworkCore;

namespace MusicLibrarySuite.Infrastructure.Data.Services;

/// <summary>
/// Represents a proxy for an instance of the <see cref="IDbContextFactory{TContextImplementation}" /> type,
/// allowing to use different service and implementation <see cref="DbContext" /> types.
/// </summary>
/// <typeparam name="TContextService">The class that will be used to resolve the context from the container.</typeparam>
/// <typeparam name="TContextImplementation">The concrete implementation type to create.</typeparam>
internal class DbContextFactoryProxy<TContextService, TContextImplementation> : IDbContextFactory<TContextService>
    where TContextService : DbContext
    where TContextImplementation : DbContext, TContextService
{
    private readonly IDbContextFactory<TContextImplementation> m_contextFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="DbContextFactoryProxy{TContextService, TContextImplementation}" /> type.
    /// </summary>
    /// <param name="contextFactory">The base <see cref="IDbContextFactory{TContextImplementation}" /> object to pass calls to.</param>
    public DbContextFactoryProxy(IDbContextFactory<TContextImplementation> contextFactory)
    {
        m_contextFactory = contextFactory;
    }

    /// <inheritdoc />
    public TContextService CreateDbContext()
    {
        return m_contextFactory.CreateDbContext();
    }
}
