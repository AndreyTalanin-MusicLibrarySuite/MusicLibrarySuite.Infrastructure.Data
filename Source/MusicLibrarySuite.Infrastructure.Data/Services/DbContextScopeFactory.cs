using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using MusicLibrarySuite.Infrastructure.Data.Services.Abstractions;

namespace MusicLibrarySuite.Infrastructure.Data.Services;

/// <summary>
/// Represents a factory for creating <see cref="IDbContextScope{TContext}" /> instances.
/// </summary>
/// <typeparam name="TContext">The <see cref="DbContext" /> type of the <see cref="IDbContextScope{TContext}" /> instance.</typeparam>
internal class DbContextScopeFactory<TContext> : IDbContextScopeFactory<TContext>
    where TContext : DbContext
{
    private readonly TContext m_context;
    private readonly IDbContextProviderSource<TContext> m_contextProviderSource;
    private readonly Stack<DbContextScope<TContext>> m_contextScopes;

    /// <summary>
    /// Initializes a new instance of the <see cref="DbContextScopeFactory{TContext}" /> type using the specified services.
    /// </summary>
    /// <param name="context">The <see cref="DbContext" /> instance to provide access to.</param>
    /// <param name="contextProviderSource">The <see cref="IDbContextProviderSource{TContext}" /> instance.</param>
    public DbContextScopeFactory(TContext context, IDbContextProviderSource<TContext> contextProviderSource)
    {
        m_context = context;
        m_contextProviderSource = contextProviderSource;
        m_contextScopes = new Stack<DbContextScope<TContext>>();
    }

    /// <inheritdoc />
    public void WithDbContext(RepositoryAction repositoryAction, TContext context)
    {
        using IDbContextScope<TContext> contextScope = CreateDbContextScope(context);
        repositoryAction();
    }

    /// <inheritdoc />
    public TResult WithDbContext<TResult>(RepositoryQuery<TResult> repositoryQuery, TContext context)
    {
        using IDbContextScope<TContext> contextScope = CreateDbContextScope(context);
        return repositoryQuery();
    }

    /// <inheritdoc />
    public async Task WithDbContextAsync(AsyncRepositoryAction repositoryAction, TContext context, CancellationToken cancellationToken = default)
    {
        using IDbContextScope<TContext> contextScope = CreateDbContextScope(context);
        await repositoryAction(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<TResult> WithDbContextAsync<TResult>(AsyncRepositoryQuery<TResult> repositoryQuery, TContext context, CancellationToken cancellationToken = default)
    {
        using IDbContextScope<TContext> contextScope = CreateDbContextScope(context);
        return await repositoryQuery(cancellationToken);
    }

    /// <inheritdoc />
    public IDbContextScope<TContext> CreateDbContextScope(TContext context)
    {
        m_contextProviderSource.Context = context;
        DbContextScope<TContext> contextScope = new(context, this);
        m_contextScopes.Push(contextScope);
        return contextScope;
    }

    /// <summary>
    /// Cancels the <see cref="DbContext" /> instance substitution configured by the scope.
    /// </summary>
    /// <remarks>If multiple nested scopes are created, they must be disposed in the reverse order as they were created.</remarks>
    /// <param name="contextScope">The scope to cancel the <see cref="DbContext" /> instance substitution for.</param>
    /// <exception cref="InvalidOperationException">Thrown if there are nested scopes that have not been disposed.</exception>
    public void DisposeDbContextScope(DbContextScope<TContext> contextScope)
    {
        DbContextScope<TContext> currentContextScope = m_contextScopes.Peek();
        if (contextScope != currentContextScope)
            throw new InvalidOperationException("Unable to dispose the enclosing scope if its nested scopes have not been disposed.");

        m_contextScopes.Pop();
        m_contextProviderSource.Context = m_contextScopes.TryPeek(out DbContextScope<TContext>? baseContextScope)
            ? baseContextScope.Context
            : m_context;
    }
}
