using System.Threading;
using System.Threading.Tasks;

namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Encapsulates an asynchronous repository query that returns a value of the <typeparamref name="TResult" /> type.
/// </summary>
/// <remarks>The repository instance to execute the query on and query parameters should be provided via closures.</remarks>
/// <typeparam name="TResult">The type of the return value of the repository query.</typeparam>
/// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
/// <returns>
/// A task that represents the asynchronous repository operation.
/// The task result contains the return value of the repository query.
/// </returns>
public delegate Task<TResult> AsyncRepositoryQuery<TResult>(CancellationToken cancellationToken = default);
