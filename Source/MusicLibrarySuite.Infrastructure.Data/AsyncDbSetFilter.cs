using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Encapsulates an asynchronous <see cref="DbSet{TEntity}" /> filter that processes an existing <see cref="IQueryable{T}" /> collection
/// containing entities of the <typeparamref name="TEntity" /> type and returns the modified collection.
/// </summary>
/// <typeparam name="TEntity">The type of entity in the <see cref="IQueryable{T}" /> collection.</typeparam>
/// <param name="collection">The existing <see cref="IQueryable{T}" /> collection.</param>
/// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
/// <returns>
/// A task that represents the asynchronous repository operation.
/// The task result contains the processed <see cref="IQueryable{T}" /> collection.
/// </returns>
public delegate Task<IQueryable<TEntity>> AsyncDbSetFilter<TEntity>(IQueryable<TEntity> collection, CancellationToken cancellationToken = default);
