using System.Linq;

using Microsoft.EntityFrameworkCore;

namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Encapsulates a <see cref="DbSet{TEntity}" /> filter that processes an existing <see cref="IQueryable{T}" /> collection
/// containing entities of the <typeparamref name="TEntity" /> type and returns the modified collection.
/// </summary>
/// <typeparam name="TEntity">The type of entity in the <see cref="IQueryable{T}" /> collection.</typeparam>
/// <param name="collection">The existing <see cref="IQueryable{T}" /> collection.</param>
/// <returns>The processed <see cref="IQueryable{T}" /> collection.</returns>
public delegate IQueryable<TEntity> DbSetFilter<TEntity>(IQueryable<TEntity> collection);
