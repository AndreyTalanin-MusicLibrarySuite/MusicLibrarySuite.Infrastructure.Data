using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Provides a set of extension methods for the <see cref="IQueryable" /> interface.
/// </summary>
public static class QueryableExtensions
{
    /// <summary>
    /// Processes an existing <see cref="IQueryable{T}" /> collection containing entities of the <typeparamref name="TEntity" /> type
    /// with the provided <see cref="DbSetFilter{TEntity}" /> filter and returns the modified collection.
    /// <para>
    /// The collection is expected to be internally represented by the <see cref="DbSet{TEntity}" /> object.
    /// </para>
    /// </summary>
    /// <typeparam name="TEntity">The type of entity in the <see cref="IQueryable{T}" /> collection.</typeparam>
    /// <param name="collection">The existing <see cref="IQueryable{T}" /> collection.</param>
    /// <param name="filter">The <see cref="DbSetFilter{TEntity}" /> filter object.</param>
    /// <returns>The processed <see cref="IQueryable{T}" /> collection.</returns>
    public static IQueryable<TEntity> Filter<TEntity>(this IQueryable<TEntity> collection, DbSetFilter<TEntity> filter)
        where TEntity : class
    {
        IQueryable<TEntity> queryable = filter(collection);
        return queryable;
    }

    /// <summary>
    /// Asynchronously processes <see cref="IQueryable{T}" /> collection containing entities of the <typeparamref name="TEntity" /> type
    /// with the provided <see cref="AsyncDbSetFilter{TEntity}" /> filter and returns the modified collection.
    /// <para>
    /// The collection is expected to be internally represented by the <see cref="DbSet{TEntity}" /> object.
    /// </para>
    /// </summary>
    /// <typeparam name="TEntity">The type of entity in the <see cref="IQueryable{T}" /> collection.</typeparam>
    /// <param name="collection">The existing <see cref="IQueryable{T}" /> collection.</param>
    /// <param name="asyncFilter">The <see cref="AsyncDbSetFilter{TEntity}" /> filter object.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>The processed <see cref="IQueryable{T}" /> collection.</returns>
    public static async Task<IQueryable<TEntity>> FilterAsync<TEntity>(this IQueryable<TEntity> collection, AsyncDbSetFilter<TEntity> asyncFilter, CancellationToken cancellationToken = default)
        where TEntity : class
    {
        IQueryable<TEntity> queryable = await asyncFilter(collection, cancellationToken);
        return queryable;
    }
}
