using System;
using System.Diagnostics.CodeAnalysis;

namespace MusicLibrarySuite.Infrastructure.Data.Helpers;

/// <summary>
/// Provides a set of database-related static methods for processing <see cref="DBNull" /> objects.
/// </summary>
public static class DbValueConvert
{
    /// <summary>
    /// Processes a nullable value so it can be used as a database value.
    /// </summary>
    /// <param name="value">The nullable value to process.</param>
    /// <returns>The value itself if it is not a <see langword="null" /> reference, the <see cref="DBNull.Value" /> object otherwise.</returns>
    public static object ToDbValue(object? value)
    {
        return value ?? DBNull.Value;
    }

    /// <summary>
    /// Casts an <see cref="object" /> value to the specified value type.
    /// </summary>
    /// <typeparam name="T">The destination value type.</typeparam>
    /// <param name="value">The value to cast.</param>
    /// <returns>
    /// The value casted to the <typeparamref name="T" /> type if it is neither a <see langword="null" /> reference nor the <see cref="DBNull.Value" /> object,
    /// the default value for the destination value type otherwise.
    /// </returns>
    /// <exception cref="InvalidCastException">Thrown if the value can not be casted to the destination type.</exception>
    public static T ToValueType<T>(object? value)
        where T : struct
    {
        return IsNullOrDbNullValue(value) ? default(T) : (T)value;
    }

    /// <summary>
    /// Casts an <see cref="object" /> value to the specified nullable value type.
    /// </summary>
    /// <typeparam name="T">The destination nullable value type.</typeparam>
    /// <param name="value">The value to cast.</param>
    /// <returns>
    /// The value casted to the <typeparamref name="T" /> type if it is neither a <see langword="null" /> reference nor the <see cref="DBNull.Value" /> object,
    /// the default value for the destination nullable value type otherwise.
    /// </returns>
    /// <exception cref="InvalidCastException">Thrown if the value can not be casted to the destination type.</exception>
    public static T? ToNullableValueType<T>(object? value)
        where T : struct
    {
        return IsNullOrDbNullValue(value) ? default(T?) : (T?)value;
    }

    /// <summary>
    /// Casts an <see cref="object" /> value to the specified reference type.
    /// </summary>
    /// <typeparam name="T">The destination reference type.</typeparam>
    /// <param name="value">The value to cast.</param>
    /// <returns>
    /// The value casted to the <typeparamref name="T" /> type if it is neither a <see langword="null" /> reference nor the <see cref="DBNull.Value" /> object,
    /// the default value for the destination reference type otherwise.
    /// </returns>
    /// <exception cref="InvalidCastException">Thrown if the value can not be casted to the destination type.</exception>
    public static T? ToReferenceType<T>(object? value)
        where T : class
    {
        return IsNullOrDbNullValue(value) ? default(T?) : (T?)value;
    }

    private static bool IsNullOrDbNullValue([NotNullWhen(false)] object? value)
    {
        return value is null || ReferenceEquals(value, DBNull.Value);
    }
}
