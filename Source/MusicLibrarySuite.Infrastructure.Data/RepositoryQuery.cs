namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Encapsulates a repository query that returns a value of the <typeparamref name="TResult" /> type.
/// </summary>
/// <remarks>The repository instance to execute the query on and query parameters should be provided via closures.</remarks>
/// <typeparam name="TResult">The type of the return value of the repository query.</typeparam>
/// <returns>The return value of the repository query.</returns>
public delegate TResult RepositoryQuery<TResult>();
