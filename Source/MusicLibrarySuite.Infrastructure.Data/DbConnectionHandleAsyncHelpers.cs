using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Provides a set of static methods implementing async support for the <see cref="DbConnectionHandle" /> class.
/// </summary>
public static class DbConnectionHandleAsyncHelpers
{
    /// <summary>
    /// Asynchronously creates a new <see cref="DbConnectionHandle" /> connection handle using the specified <see cref="DbConnection" /> connection object.
    /// </summary>
    /// <param name="connection">The <see cref="DbConnection" /> connection object.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>
    /// The task object representing the asynchronous operation.
    /// The task's result will be the <see cref="DbConnectionHandle" /> connection handle created.
    /// </returns>
    public static async Task<DbConnectionHandle> CreateDbConnectionHandleAsync(DbConnection connection, CancellationToken cancellationToken = default)
    {
        bool closeConnectionOnDispose = false;
        if (connection.State == ConnectionState.Closed)
        {
            await connection.OpenAsync(cancellationToken);
            closeConnectionOnDispose = true;
        }

        try
        {
            return new DbConnectionHandle(connection, closeConnectionOnDispose);
        }
        catch
        {
            if (closeConnectionOnDispose)
                await connection.CloseAsync();
            throw;
        }
    }
}
