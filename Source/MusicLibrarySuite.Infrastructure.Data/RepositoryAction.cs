namespace MusicLibrarySuite.Infrastructure.Data;

/// <summary>
/// Encapsulates a repository action.
/// </summary>
/// <remarks>The repository instance to execute the action on and action parameters should be provided via closures.</remarks>
public delegate void RepositoryAction();
