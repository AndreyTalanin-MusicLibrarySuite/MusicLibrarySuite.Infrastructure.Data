using System;

using Microsoft.Data.SqlClient;

using MusicLibrarySuite.Infrastructure.Exceptions.Delegates;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;

namespace MusicLibrarySuite.Infrastructure.Data.SqlServer.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IExceptionMapperBuilder" /> interface.
/// </summary>
public static class ExceptionMapperBuilderExtensions
{
    /// <summary>
    /// Adds a new exception mapping rule for <see cref="SqlException" /> exceptions.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance.</param>
    /// <param name="sqlErrorPredicate">A predicate checking whether a thrown <see cref="SqlException" /> exception containing a <see cref="SqlError" /> error should be mapped.</param>
    /// <param name="factoryMethod">A factory method creating a new mapped exception, providing the original <see cref="SqlException" /> exception as context.</param>
    public static void AddSqlExceptionRule(this IExceptionMapperBuilder exceptionMapperBuilder, Predicate<SqlError> sqlErrorPredicate, ExceptionFactoryMethod<SqlException> factoryMethod)
    {
        bool Predicate(SqlException sqlException)
        {
            bool result = false;
            foreach (SqlError sqlError in sqlException.Errors)
            {
                if (result |= sqlErrorPredicate(sqlError))
                    break;
            }

            return result;
        }

        exceptionMapperBuilder.AddRule(Predicate, factoryMethod);
    }

    /// <summary>
    /// Adds a new exception mapping rule for <see cref="SqlException" /> exceptions.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance.</param>
    /// <param name="sqlErrorClass">An SQL error class. See <see cref="SqlError.Class" /> for more details.</param>
    /// <param name="sqlErrorNumber">An SQL error number. See <see cref="SqlError.Number" /> for more details.</param>
    /// <param name="sqlErrorPredicate">A predicate checking whether a thrown <see cref="SqlException" /> exception containing a <see cref="SqlError" /> error should be mapped.</param>
    /// <param name="factoryMethod">A factory method creating a new mapped exception, providing the original <see cref="SqlException" /> exception as context.</param>
    public static void AddSqlExceptionRule(this IExceptionMapperBuilder exceptionMapperBuilder, int sqlErrorClass, int sqlErrorNumber, Predicate<SqlError> sqlErrorPredicate, ExceptionFactoryMethod<SqlException> factoryMethod)
    {
        bool SqlErrorPredicate(SqlError sqlError) => sqlError.Class == sqlErrorClass && sqlError.Number == sqlErrorNumber && sqlErrorPredicate(sqlError);

        exceptionMapperBuilder.AddSqlExceptionRule(SqlErrorPredicate, factoryMethod);
    }
}
